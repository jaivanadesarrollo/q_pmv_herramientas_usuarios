plugins {
    java
    id("io.quarkus")
}

repositories {
    mavenCentral()
    mavenLocal()
    maven {
        url = uri("https://admin.jaivana.co/remoteRepo/")
        //isAllowInsecureProtocol = true
    }
}

val quarkusPlatformGroupId: String by project
val quarkusPlatformArtifactId: String by project
val quarkusPlatformVersion: String by project

dependencies {
    implementation(enforcedPlatform("${quarkusPlatformGroupId}:${quarkusPlatformArtifactId}:${quarkusPlatformVersion}"))
    implementation("io.quarkus:quarkus-smallrye-reactive-messaging-kafka")
    implementation("log4j:log4j:1.2.17")
    implementation("io.quarkus:quarkus-kafka-client")
    implementation("io.quarkus:quarkus-reactive-pg-client:2.5.2.Final")
    implementation("co.jaivana:pmv_library:1.0.22-SNAPSHOT")
    implementation("co.jaivana:pmv_modelos:1.0.75-SNAPSHOT") 
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml:2.13.2")
    
}

group = "jaivana.http"
version = "1.0.0-SNAPSHOT"

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
    options.compilerArgs.add("-parameters")
}
