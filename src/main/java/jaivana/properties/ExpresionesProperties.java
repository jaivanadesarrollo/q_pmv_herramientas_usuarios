package jaivana.properties;

import java.util.Map;

import io.smallrye.config.ConfigMapping;

@ConfigMapping(prefix = "expresiones")
public interface ExpresionesProperties {
    Map<String, String> properties();
}
