package jaivana.properties;

import java.util.Map;

import io.smallrye.config.ConfigMapping;

@ConfigMapping(prefix = "jaivana")
public interface ApplicationProperties {
    Map<String, String> variable();
}
