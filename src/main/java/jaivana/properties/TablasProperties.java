package jaivana.properties;

import java.util.Map;

import io.smallrye.config.ConfigMapping;

@ConfigMapping(prefix = "tablas")
public interface TablasProperties {
    Map<String, String> properties();
}

