package jaivana.properties;

import java.util.Map;

import io.smallrye.config.ConfigMapping;

@ConfigMapping(prefix = "conexion")
public interface ConexionProperties {
    Map<String, String> database();
}
