package jaivana.http;

import java.util.Properties;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.log4j.Logger;
import org.eclipse.microprofile.reactive.messaging.Incoming;

import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import io.smallrye.mutiny.vertx.core.AbstractVerticle;
import io.smallrye.reactive.messaging.kafka.KafkaClientService;
import io.smallrye.reactive.messaging.kafka.KafkaProducer;
import io.vertx.core.json.JsonObject;
import io.vertx.mutiny.core.Promise;
import io.vertx.core.json.JsonArray;
import jaivana.properties.ApplicationProperties;
import jaivana.properties.ConexionProperties;
import jaivana.properties.ExpresionesProperties;

import java.util.HashMap;
import io.vertx.pgclient.PgConnectOptions;
import io.vertx.pgclient.PgPool;
import java.util.ArrayList;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import io.vertx.sqlclient.PoolOptions;

import jaivana.anotaciones.DBField;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Date;
import java.util.Calendar;

import java.util.Map;

@ApplicationScoped
public class HerramientasUsuariosResource extends BasicoResource {

    void onStart(@Observes StartupEvent ev) {

        ConexionBasica.setProperties(application_properties.variable(), null);

        nombreTopic = application_properties.variable().get("nombreTopic");
        utilsTopic = application_properties.variable().get("utilsTopic");

        Properties p = new Properties();
        p.setProperty("log4j.rootLogger", "ALL, RollingAppender");
        p.setProperty("log4j.appender.RollingAppender", "org.apache.log4j.DailyRollingFileAppender");
        p.setProperty("log4j.appender.RollingAppender.File", "logs//"+nombreTopic+".log");
        p.setProperty("log4j.appender.RollingAppender.DatePattern", "'-'yyyy-MM-dd'.log'");
        p.setProperty("log4j.appender.RollingAppender.layout", "org.apache.log4j.PatternLayout");
        p.setProperty("log4j.appender.RollingAppender.layout.ConversionPattern", "[%p] %d %c %M - %m%n");
        
        org.apache.log4j.PropertyConfigurator.configure(p);

        jaivana.anotaciones.Expresiones.addExpresion();

        //logger.info("[TOKEN] ---> "+token);

        pgPool = PgPool.pool(new PgConnectOptions()
        .setHost(lista_database.database().get("url"))
        .setDatabase(lista_database.database().get("database"))
        .setUser(lista_database.database().get("user"))
        .setPassword(lista_database.database().get("password"))
        .setPort(Integer.parseInt(lista_database.database().get("port"))), 
        new PoolOptions().setMaxSize(Integer.parseInt(lista_database.database().get("pool_size"))));

        pgPool.getConnection(onItemCallback -> {
            onItemCallback.result().query("SELECT 'ok'").execute().onSuccess(result -> {
                logger.info("[] Conexion exitosa!!!");
                onItemCallback.result().close();
            });
        });

        bDatos.add(new jaivana.datos.postgres.Conexion(pgPool, clientService, nombreTopic));
        nConexion++;

        losModelos.put("13",new jaivana.models.pmv.pmv_herramientas_usuarios.Usuarios());
        losModelos.put("100",new jaivana.models.pmv.pmv_herramientas_usuarios.Secciones());
        losModelos.put("101",new jaivana.models.pmv.pmv_herramientas_usuarios.Formularios());
        losModelos.put("102",new jaivana.models.pmv.pmv_herramientas_usuarios.CamposFormularios());
        losModelos.put("103",new jaivana.models.pmv.pmv_herramientas_usuarios.Servicios());
        losModelos.put("104",new jaivana.models.pmv.pmv_herramientas_usuarios.ServiciosFormularios());
        losModelos.put("105",new jaivana.models.pmv.pmv_herramientas_usuarios.SeccionesFormularios());

        losModelos.put("108",new jaivana.models.pmv.pmv_herramientas_usuarios.Roles());
        
        losModelos.put("13001",new jaivana.models.pmv.pmv_herramientas_usuarios.UsuariosCambiarClave());
        losModelos.put("13002",new jaivana.models.pmv.pmv_herramientas_usuarios.UsuariosActivarInactivar()); 

        losModelos.put("109",new jaivana.models.pmv.pmv_herramientas_usuarios.RolesPermisos()); 
        losModelos.put("110",new jaivana.models.pmv.pmv_herramientas_usuarios.UsuariosPermisos()); 
        losModelos.put("111",new jaivana.models.pmv.pmv_herramientas_usuarios.UsuariosRoles()); 

    }


    @Incoming("pmv_herramientas_usuarios")
    public void procesarConsulta(ConsumerRecord<String, String> record) 
    {
        
        System.out.println("Entro peticion...pmv_herramientas usuarios");
        JsonObject datosAdicionales=new JsonObject();
        JsonObject body= new JsonObject(record.value());
        JsonObject generales = body.getJsonObject("generales");
        String tipoServicio = generales.getString("tipo_servicio");
        if(losModelos.get(tipoServicio)!=null) 
        {
            if(tipoServicio.equals("13")) //usuarios
            {
                if(generales.getString("operacion").equals("5")) 
                {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Calendar c = Calendar.getInstance();
                    c.add(Calendar.DATE, 30);  // number of days to add
                    String dt = sdf.format(c.getTime()); 
                    //crea
                    datosAdicionales.put("usuarios_fecha_creacion",new SimpleDateFormat("yyyy-MM-dd").
                                                                       format(Calendar.getInstance().getTime())); 
                    datosAdicionales.put("usuarios_fecha_vencimiento",dt); //hoy mas 30
                    datosAdicionales.put("usuarios_llave_1","primero");
                    datosAdicionales.put("usuarios_llave_2","primero");
                    datosAdicionales.put("usuarios_fecha_ultimo_cambio_clave",new SimpleDateFormat("yyyy-MM-dd").
                                                                       format(Calendar.getInstance().getTime()));
                    System.out.println("paso por usuarios...");
                    ejecutarPeticion(record,losModelos.get(tipoServicio),datosAdicionales);
                }
                else if(generales.getString("operacion").equals("6")) 
                {
                    ejecutarPeticion(record,losModelos.get(tipoServicio));
                } 
                else 
                {
                  ejecutarPeticion(record,losModelos.get(tipoServicio));
                }   
            }
            else if(tipoServicio.equals("13001")) //usuarios
            {
                //buscra id
                if(generales.getString("operacion").equals("2")) 
                {
                   System.out.println("entre a la busqueda del id");
                   datosAdicionales.put("usuarios_id",generales.getJsonObject("usuario").getInteger("usuarios_id"));
                   ejecutarPeticion(record,losModelos.get(tipoServicio),datosAdicionales);
                }
                else if(generales.getString("operacion").equals("6_1")) 
                {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Calendar c = Calendar.getInstance();
                    c.add(Calendar.DATE, 30);  // number of days to add
                    String dt = sdf.format(c.getTime()); 

                    datosAdicionales.put("usuarios_id",generales.getJsonObject("usuario").getInteger("usuarios_id"));
                    datosAdicionales.put("usuarios_fecha_vencimiento",dt); //hoy mas 30
                    datosAdicionales.put("usuarios_fecha_ultimo_cambio_clave",new SimpleDateFormat("yyyy-MM-dd").
                                                                       format(Calendar.getInstance().getTime()));
                    System.out.println("paso por usuarios.13001..");
                    ejecutarPeticion(record,losModelos.get(tipoServicio),datosAdicionales);
                }  
                else if(generales.getString("operacion").equals("6_2")) 
                {
                    JsonObject datos = body.getJsonObject("datos");

                    String sSqlUsuariosClave = "SELECT usuario_id,llave_1,llave_2 FROM usuarios_cambios_clave WHERE llave='"+datos.getString("token")+"' and estado='P'";
                    body.put("sql",sSqlUsuariosClave);
                    System.out.println("query 2 :: "+sSqlUsuariosClave);
                    Promise<JsonObject> ftClave = Promise.promise();

                    bDatos.get(0).buscarConCondicionNuevoPMV(body, ftClave);

                    ftClave.future().subscribe().with(handler -> 
                    {
                        if(handler.getInteger("estado_p") == 200){

                            JsonObject data = handler.getJsonArray("data").getJsonObject(0);
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                            Calendar c = Calendar.getInstance();
                            c.add(Calendar.DATE, 30);  // number of days to add
                            String dt = sdf.format(c.getTime()); 

                            String sSqlUpdateUsuarios = "UPDATE usuarios SET fecha_ultimo_cambio_clave=now(),llave_1='"+
                                                        data.getString("llave_1")+"',llave_2='"+data.getString("llave_2")+
                                                        "',fecha_vencimiento='"+dt+"' WHERE id="+data.getInteger("usuario_id");
                            body.put("sql", sSqlUpdateUsuarios);
                            System.out.println("query :: "+sSqlUpdateUsuarios);
                            Promise<JsonObject> ftUpdate = Promise.promise();
                            bDatos.get(0).modificarDatoPMV(body, ftUpdate);

                            ftUpdate.future().subscribe().with(handler2 -> 
                            {
                                if(handler2.getInteger("estado_p") == 200){
                                    String sSqlUpdateCambioClave = "UPDATE usuarios_cambios_clave SET estado='X' WHERE llave='"+
                                                                   datos.getString("token")+"'";
                                    body.put("sql", sSqlUpdateCambioClave);

                                    bDatos.get(0).modificarDatoPMV(generales.getString("idResponse"), body);
                                }else{
                                    sendResponse(handler2);  
                                }
                            });
                        }else{
                           sendResponse(handler);
                        }
                    });
                }   
                else {
                   ejecutarPeticion(record,losModelos.get(tipoServicio)); 
                }   
            }
            else if(tipoServicio.equals("13002")) //usuarios activar inactivar
            {
                //buscra id
                if(generales.getString("operacion").equals("6_1")) 
                {
                   System.out.println("entre a crear en el log");
                   datosAdicionales.put("usuarios_id",
                                         generales.getJsonObject("usuario").getInteger("usuarios_id"));
                    Calendar c = Calendar.getInstance();
                    c.add(Calendar.DATE, 30);  // number of days to add
                    datosAdicionales.put("usuariosactivarinactivar_fecha_sistema",new SimpleDateFormat("yyyy-MM-dd").
                                                                       format(Calendar.getInstance().getTime()));
                    ejecutarPeticion(record,losModelos.get(tipoServicio),datosAdicionales);
                }
                else {
                   ejecutarPeticion(record,losModelos.get(tipoServicio));      
                }
            }    
            else {
              ejecutarPeticion(record,losModelos.get(tipoServicio));
            }  
        }
        else 
        {
            //retornar el error
            badRequestTopicInterno(body.getJsonObject("generales").getString("IdReponse"), 
                                   body, 
                                   "Modelo no soportado");
        }    
    }

    @Incoming("pmv_herramientas_usuarios-utils-1")
    public void procesarConsultaUtils(ConsumerRecord<String, String> record) {
        JsonObject body = new JsonObject(record.value());
        JsonObject data = new JsonObject();

        data.put("estado_p",body.getInteger("estado_p"));
        if(body.getValue("data") instanceof JsonObject){
            data.put("data",body.getJsonObject("data"));
        }else{
            data.put("data",body.getJsonArray("data"));
        }
        if(body.containsKey("mensaje")){
            data.put("mensaje", body.getString("mensaje"));
        }

        data.put("topic_gateway", body.getString("topic_gateway"));

        data.put("generales", body.getJsonObject("generales"));
        if(ConexionBasica.promises.get(body.getString("est_pro")) != null){
            ConexionBasica.promises.get(body.getString("est_pro")).tryComplete(data);
            ConexionBasica.promises.remove(body.getString("est_pro"));
        }
    }


}